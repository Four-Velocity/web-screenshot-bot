#! /bin/sh

### BEGIN INIT INFO
# Provides:        main.py
# Required-Start:  $remote_fs $syslog
# Required-Stop:   $remote_fs $syslog
# Default-Start    2 3 4 5
# Default-Stop:    0 1 6
### END INIT INFO

update() {
  echo "Updating server!"
  cd ~/server/ || exit
  pipenv install --dev
  python_path=$(pipenv run which python3)
  echo "#! $python_path" | cat - main.py > temp && mv temp main.py
  chmod +x main.py
}

start() {
  echo "Starting webhook server!"
  ~/server/main.py &
}

stop() {
  echo "Stopping webhook server!"
  pkill -f ~/server/main.py &
}

case "$1" in
  start)
    start
    ;;
  update-start)
    update
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  reboot)
    stop
    update
    start
    ;;
  *)
    echo "Usage: ~/server/scripts/on_off.sh {start|update-start|stop|restart|reboot}"
    exit 1
    ;;
esac
