from app.app import app
from app.bot import bot
import app.config as cfg
from time import sleep


if __name__ == "__main__":
    # Resetting bot webhook
    bot.remove_webhook()
    sleep(1)
    bot.set_webhook(
        url=cfg.WEBHOOK_URL_BASE + cfg.WEBHOOK_URL_PATH,
        certificate=open(cfg.WEBHOOK_SSL_CERT, "r"),
    )
    # Run webhook server
    app.run(
        host="0.0.0.0",
        debug=True,
        ssl_context=(cfg.WEBHOOK_SSL_CERT, cfg.WEBHOOK_SSL_PRIV),
    )
