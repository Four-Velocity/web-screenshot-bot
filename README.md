# Web Screenshot Bot
[![Telegram Bot](https://img.shields.io/website?down_color=E05D44&down_message=ofline&label=Telegram%20Bot&logo=telegram&labelColor=0088CC&style=flat-square&up_color=65CD2A&up_message=online&url=http%3A%2F%2F176.36.99.182%3A443%2F)](http://t.me/web_screener_bot)
[![Build status](https://gitlab.com/Four-Velocity/web-screenshot-bot/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/Four-Velocity/web-screenshot-bot/-/pipelines)
[![Code Style Black](https://img.shields.io/badge/code%20style-black-black.svg?style=flat-square)](https://github.com/psf/black)

This is a simple telegram bot that will make a web page screenshots for you
### Info
Click the badge to open the bot :point_up:  
To use this bot you should send him a **full** url.  
The bot will make a screenshot and send a photo and a file without compression to you.  
If anybody ever sent shuch a link the bot can send you the old one screenshot or make a new one for you.  
Using command `/last` you can ask a bot to send yours last web page screenshot.


If the bot is ofline try to use it, I've some troubles with this bage sometimes.  
If the bot still silent message me and I'll turn it on. [Telegram: @Four-Velocity](t.me/Four_Velocity)