import os
from time import sleep

import telebot
from selenium.common.exceptions import InvalidArgumentException as IAE
from telebot import types

import app.config as cfg
from app.utils.mongo_operations import DbWorker
from app.utils.other import find_url
from app.utils.screenshots import do_screenshot

# Create bot instance
bot = telebot.TeleBot(cfg.BOT_TOKEN)
# Create worker instance
worker = DbWorker()


# Command handlers
@bot.message_handler(commands=["start"])
def greetings(message):
    """Simple greetings."""
    bot.send_message(
        message.chat.id,
        "Arigato! Send me some URLs pls, so I can take photo 4U",
        reply_markup=types.ReplyKeyboardRemove(selective=False),
    )


@bot.message_handler(commands=["last"])
def last(message):
    """Get users last url and send it's screenshot."""
    user_data = worker.get_user_instance(message.chat.id)
    url_instance = worker.get_image_instance(user_data["url"])
    send_old(
        message.chat.id,
        url_instance["photo_id"],
        url_instance["file_id"],
        user_data["url"],
    )


# Url's handler
@bot.message_handler(content_types=["text"])
def get_url(message):
    """Proceed user url."""
    url = find_url(message.text)
    if url:
        url_data = worker.get_image_instance(url)
        try:
            already_cached(message, url_data)
        except (TypeError, KeyError):
            screenshot(message.chat.id, url)
    else:
        bot.send_message(
            message.chat.id, "I can't find any URLs here, please try againg"
        )


# Callback handler
@bot.callback_query_handler(func=lambda call: True)
def callback(call):
    """Simple callback handler."""
    if call.message:
        # Create tuple from string. *Yeah I know it's not very safe*
        data = eval(call.data)
        user_data = worker.get_user_instance(data[1])
        if data[0]:
            screenshot(data[1], user_data["url"])
        else:
            image_data = worker.get_image_instance(user_data["url"])
            send_old(
                data[1], image_data["photo_id"], image_data["file_id"], user_data["url"]
            )


# Additional functions
def already_cached(message, url_data):
    """Try to get url from db."""
    url = url_data["url"]
    worker.update_or_inserted_user_instance(message.chat.id, url)
    markup = types.InlineKeyboardMarkup()
    pattern = "({}, {})"
    send = types.InlineKeyboardButton(
        text="Send old", callback_data=pattern.format(False, message.chat.id)
    )
    update = types.InlineKeyboardButton(
        text="Retake", callback_data=pattern.format(True, message.chat.id)
    )
    markup.add(send, update)
    bot.send_message(
        message.chat.id,
        "This web page screen was already made.\nDo you want to make a new one?",
        reply_markup=markup,
    )


def screenshot(chat_id, url):
    """Do screenshot, send it to user, save it id to db."""
    try:
        bot.send_message(chat_id, f"Screening {url}")
        save = os.path.join(cfg.BASE_DIR, "screens", f"{chat_id}.png")
        do_screenshot(url, save_path=save)
        with open(save, "rb") as photo:
            photo_msg = bot.send_photo(chat_id, photo, url)
        with open(save, "rb") as photo:
            file_msg = bot.send_document(chat_id, photo)
        worker.update_or_insert_image_instance(
            url, photo_msg.photo[-1].file_id, file_msg.document.file_id
        )
        worker.update_or_inserted_user_instance(chat_id, url)
        os.remove(save)
    except IAE:
        bot.send_message(chat_id, f"I don't think \"{url}\" is valid URL, I'll pass it")
    except Exception as e:
        print(e)
        sleep(1)
        bot.send_message(chat_id, f"I can't screen \"{url}\" \nSowwy🥺")


def send_old(chat_id, photo_id, file_id, url):
    """Send screenshot by id."""
    bot.send_photo(chat_id, photo_id, url)
    bot.send_document(chat_id, file_id)
