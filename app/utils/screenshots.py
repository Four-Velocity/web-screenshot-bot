from time import sleep

from selenium import webdriver


def do_screenshot(url, save_path):
    """Make screenshot of the given page."""
    # Set up webdriver options
    options = webdriver.FirefoxOptions()
    options.add_argument("--star-maximized")
    options.add_argument("--start-fullscreen")
    options.add_argument("--headless")
    # Set up driver
    driver = webdriver.Firefox(options=options)
    driver.set_script_timeout(30)
    # Get web page
    driver.get(url)
    # Get <body> height
    auto_height = driver.execute_script("return document.body.scrollHeight")
    # Increase height if it's too small
    if auto_height <= 1080:
        auto_height = 1080
    # Or scroll down the page if needed
    else:
        while True:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            sleep(0.5)
            new_height = driver.execute_script("return document.body.scrollHeight")
            if new_height == auto_height:
                break
            auto_height = new_height
    driver.set_window_size(1920, auto_height)
    # Make <body> screenshot
    driver.find_element_by_tag_name("body").screenshot(save_path)
    # Stop driver
    driver.quit()
