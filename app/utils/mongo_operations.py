import app.config as cfg
from pymongo import MongoClient


class DbWorker:
    """Contain operations with MongoDB."""

    def __init__(self):
        """Create mongo connection using config data."""
        client = MongoClient(cfg.MONGO_URL)
        db = client[cfg.MONGO_DB]
        self.users = db[cfg.MONGO_USERS]
        self.store = db[cfg.MONGO_STORAGE]

    def get_image_instance(self, url):
        """Return info about the image or None."""
        return self.store.find_one({"url": url})

    def update_or_insert_image_instance(self, url, photo_id, file_id):
        """Update info about existing image or creates new."""
        self.store.update_one(
            {"url": url},
            {"$set": {"photo_id": photo_id, "file_id": file_id}},
            upsert=True,
        )

    def get_user_instance(self, user_id):
        """Return info about the user or None."""
        return self.users.find_one({"user": user_id})

    def update_or_inserted_user_instance(self, user_id, url):
        """Update info about existing user or creates new."""
        self.users.update_one({"user": user_id}, {"$set": {"url": url}}, upsert=True)
