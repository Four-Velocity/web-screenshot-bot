import telebot
from flask import Flask
from flask import request

import app.config as cfg
from app.bot import bot

app = Flask(__name__)


@app.route("/")
def index():
    """Main page endpoint."""
    return "Flask is up"


@app.route(cfg.WEBHOOK_URL_PATH, methods=["POST"])
def webhook():
    """Webhook endpoint."""
    try:
        if request.headers["content-type"] == "application/json":
            json_string = request.json
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
            return ""
    except KeyError:
        return
