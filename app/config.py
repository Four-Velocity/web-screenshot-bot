import os
import toml

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Load config from file
config = toml.load(os.path.join(BASE_DIR, "config.toml"))

BOT_TOKEN = config["bot"]["token"]

WEBHOOK_HOST = config["webhook"]["host"]
WEBHOOK_PORT = config["webhook"]["port"]
WEBHOOK_LISTEN = config["webhook"]["listen"]

WEBHOOK_SSL_CERT = os.path.join(BASE_DIR, config["webhook"]["ssl_cert"])
WEBHOOK_SSL_PRIV = os.path.join(BASE_DIR, config["webhook"]["ssl_priv"])

WEBHOOK_URL_BASE = f"https://{WEBHOOK_HOST}:{WEBHOOK_PORT}"
WEBHOOK_URL_PATH = f"/{BOT_TOKEN}/"


MONGO_TYPE = config["mongo"]["connection"]["type"]
MONGO_USER = config["mongo"]["connection"]["user"]
MONGO_PASSWORD = config["mongo"]["connection"]["password"]
MONGO_IP = config["mongo"]["connection"]["ip"]
MONGO_PORT = config["mongo"]["connection"]["port"]
MONGO_DB = config["mongo"]["namespace"]["db"]
MONGO_USERS = config["mongo"]["namespace"]["users_collection"]
MONGO_STORAGE = config["mongo"]["namespace"]["storage_collection"]

MONGO_URL = "mongodb{}://{}:{}@{}/?retryWrites=true&w=majority"
if MONGO_TYPE == "atlas":
    MONGO_URL = MONGO_URL.format("+srv", MONGO_USER, MONGO_PASSWORD, MONGO_IP)
elif MONGO_TYPE == "local":
    MONGO_URL = MONGO_URL.format(
        "", MONGO_USER, MONGO_PASSWORD, f"{MONGO_IP}:{MONGO_PORT}"
    )
else:
    raise ValueError("Incorrect Mongo type in config")
